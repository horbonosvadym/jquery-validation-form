<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Form js component</title>
  <?php require_once($_SERVER['DOCUMENT_ROOT'] . '/scripts/get_bundle_name.php') ?>

  <link rel="stylesheet" href="<?php echo get_bundle_name()['css'] ?>">
</head>
<body>
  <div class="o-site">
    <div class="o-flex">
      <div class="o-container">
        <section>
          <h1 class="t1 u-mt u-midnight-color">Jquery form validation component</h1>
          <div class="c-form js-validate-scope u-mb">
            <form action="" novalidate>
              <div class="c-form__inner">
                <div class="c-form__group">
                  <div class="c-form__controls js-form-controls">
                    <label for="name" class="c-form__label">What's your name?</label>
                    <input 
                    nama="name" 
                    id="name" 
                    type="text"
                    class="js-validate-input" 
                    required="required"
                    data-msg="What's your name?"
                    data-rule-minlength="2"
                    data-msg-minlength="Min length demo error message"
                    >
                  </div>
                </div>
                <div class="c-form__group">
                  <div class="c-form__controls js-form-controls">
                    <label for="email" class="c-form__label">Email</label>
                    <input 
                    name="email" 
                    id="email" 
                    type="email" 
                    class="js-validate-input" 
                    required="required" 
                    data-msg="Email"
                    >
                  </div>
                </div>
                <div class="c-form__group">
                  <div class="c-form__controls js-form-controls">
                    <label for="msg" class="c-form__label">What are you interested in?</label>
                    <textarea 
                      name="msg" 
                      id="msg"
                      class="js-validate-input" 
                      data-rule-minlength="3" 
                      required="required"
                      data-msg="What are you interested in?"
                      data-msg-minlength="Really nothing to say?"
                      ></textarea>
                  </div>
                </div>
                <div class="c-form__group c-form__action js-submit">
                  <button class="c-form__btn c-btn js-submit-btn is-disabled" type="submit">Submit</button>
                </div>
                <div class="c-form__group c-errors js-error-labels js-error-container">
                  <p class="c-errors__capture">Left to fill</p>  
                  <label id="name-error" class="has-error c-errors__item" for="name">What's your name?</label>
                  <label id="email-error" class="has-error c-errors__item" for="email">Email</label>
                  <label id="msg-error" class="has-error c-errors__item" for="msg">What are you interested in?</label>
                </div>
            </form>
          </div>
        </section>
      </div>
    </div>
  </div>

  <script src="<?php echo get_bundle_name()['js'] ?>"></script>
</body>
</html>