const $ = require('jquery');

const IS_DISABLED = 'is-disabled';
const HAS_ERROR   = 'c-errors__item';

require('jquery-validation');

function validate(selector, submitBtnSelector, errorContainer, errorLabelContainer) {

  const $scopes = $(selector);
  
  if (!$scopes.length > 0)
    return;
  
  const $forms = $scopes.find('form');
    
  setupValidation();
    
  function setupValidation() {
    $forms.each(function() {
      const $btn = $(this).find(submitBtnSelector);

      $(this).validate({
        errorClass         : HAS_ERROR,
        errorContainer     : $(this).find(errorContainer),
        errorLabelContainer: $(this).find(errorLabelContainer),

        submitHandler: function($form) {
          $btn.removeClass(IS_DISABLED);
          $form.submit();
        },
        success: function($label) {
          $label.remove();
        },
        invalidHandler: function() {
          $btn.addClass(IS_DISABLED);
        }
      });

      btnDisableEnable($(this), $btn);
    });
  }

  function btnDisableEnable(form, btn) {
    const $form = form;
    const $btn  = btn;

    $form.on('change keyup', function() {
      if ($form.valid()) {
        if ($btn.hasClass(IS_DISABLED)) {
          $btn.removeClass(IS_DISABLED);
        }
      } else {
        if (!$btn.hasClass(IS_DISABLED)) {
          $btn.addClass(IS_DISABLED);
        }
      }
    });
  }
}

validate('.js-validate-scope', '.js-submit-btn', '.js-error-container', '.js-error-labels');
